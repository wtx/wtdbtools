package de.wernertoex.dbtools.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * 
 */
@MappedSuperclass
public abstract class AbstractEntity<K> implements Serializable {

	private static final long serialVersionUID = -8403919523078478338L;

	@Version
	// Version wird hochgezählt.für Optimistic Locking
	@Column(name = "version")
	private int version = 0;

	public abstract K getId();

	public abstract void setId(final K id);

	public int getVersion() {
		return this.version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}

	@Temporal(TemporalType.TIMESTAMP)
	private Date createTimeStamp;

	@Temporal(TemporalType.TIMESTAMP)
	private Date changeTimeStamp;

	public Date getCreateTimeStamp() {
		return createTimeStamp;
	}

	protected void setCreateTimeStamp(Date createTimeStamp) {
		this.createTimeStamp = createTimeStamp;
	}

	public Date getChangeTimeStamp() {
		return changeTimeStamp;
	}

	public void setChangeTimeStamp(Date changeTimeStamp) {
		this.changeTimeStamp = changeTimeStamp;
	}

	@PrePersist
	// Vor Anlage, kein Update
	@Column(nullable = false, updatable = false)
	public void prePersist() {
		Date date = new Date();
		setCreateTimeStamp(date);
		setChangeTimeStamp(date);
	}

	@PreUpdate
	public void preUpdate() {
		setChangeTimeStamp(new Date());
	}

	@Override
	public String toString() {
		String result = getClass().getSimpleName() + " ";
		if (getId() != null)
			result += "id: " + getId();
		return result;
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		if (getId() != null) {
			return getId().equals(((AbstractEntity<K>) that).getId());
		}
		return super.equals(that);
	}

	@Override
	public int hashCode() {
		if (getId() != null) {
			return getId().hashCode();
		}
		return super.hashCode();
	}

	public boolean isEntityTransient() {
		return getId() == null;
	}
}