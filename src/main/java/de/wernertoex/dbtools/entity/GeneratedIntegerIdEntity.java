package de.wernertoex.dbtools.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public  class GeneratedIntegerIdEntity extends AbstractEntity<Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1255303874964423082L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id", updatable=false,nullable=false)
	protected Integer id;
	
	@Override
	public Integer getId() {
		return id;
	}

	
	@Override
	public void setId(Integer id) {
		throw new UnsupportedOperationException();
		// Not supported	
	}

}
