package de.wernertoex.dbtools.repository;

import java.io.Serializable;

import javax.persistence.EntityManager;

import de.wernertoex.dbtools.entity.AbstractEntity;

/**
 * 
 * Stellt ein Repository zur Verwaltung von JPA - Entities zur Verfügung.
 * 
 * @author wernertoex
 *
 * @param <K> Typ des Id - Values der Entity
 * @param <E> Typ der Entity, die verwaltet werden soll.
 */
public abstract class SingleIdEntityRepository<K, E extends AbstractEntity<K>> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1803073264811003623L;

	protected EntityManager em;
	
	protected abstract void setEntityManager(EntityManager em);
	
	public E saveEntity(E entity){
		if (entity.isEntityTransient()){
			em.persist(entity);
			return entity;
		} else
			return em.merge(entity);
	}
	
	public void persist(E entity){
		em.persist(entity);
	}
	
	public E merge(E entity){
		return em.merge(entity);
	}
	
	protected abstract Class<E> getEntityClass();
	
	public E findById(K id){
		return em.find(getEntityClass(), id );
	}
	
	
}
